<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiProperty;

/**
 * ProArticlesPrix
 *
 * @ORM\Table(name="pro_articles_prix")
 * @ORM\Entity
 * @ApiResource()
 */
class ProArticlesPrix
{
    /**
     * @var string
     *
     * @ORM\Column(name="GUID", type="string", length=100, nullable=false, options={"default":""})
     * @ORM\Id
     * @ApiProperty(identifier=true)
     */
    private $guid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="AR_REF", type="string", length=18, nullable=true)
     */
    private $arRef;

    /**
     * @var int|null
     *
     * @ORM\Column(name="AG_NO1", type="integer", nullable=true)
     */
    private $agNo1;

    /**
     * @var int|null
     *
     * @ORM\Column(name="AG_NO2", type="integer", nullable=true)
     */
    private $agNo2;

    /**
     * @var int|null
     *
     * @ORM\Column(name="N_CATTARIF", type="integer", nullable=true)
     */
    private $nCattarif;

    /**
     * @var float|null
     *
     * @ORM\Column(name="PRIX", type="float", precision=10, scale=0, nullable=true)
     */
    private $prix;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CT_PRIXTTC", type="string", length=7, nullable=true)
     */
    private $ctPrixttc;

    public function getGuid(): ?string
    {
        return $this->guid;
    }

    public function setGuid(string $guid): self
    {
        $this->guid = $guid;

        return $this;
    }

    public function setArRef(?string $arRef): self
    {
        $this->arRef = $arRef;

        return $this;
    }

    public function getAgNo1(): ?int
    {
        return $this->agNo1;
    }

    public function setAgNo1(?int $agNo1): self
    {
        $this->agNo1 = $agNo1;

        return $this;
    }

    public function getAgNo2(): ?int
    {
        return $this->agNo2;
    }

    public function setAgNo2(?int $agNo2): self
    {
        $this->agNo2 = $agNo2;

        return $this;
    }

    public function getNCattarif(): ?int
    {
        return $this->nCattarif;
    }

    public function setNCattarif(?int $nCattarif): self
    {
        $this->nCattarif = $nCattarif;

        return $this;
    }

    public function getPrix(): ?float
    {
        return $this->prix;
    }

    public function setPrix(?float $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getCtPrixttc(): ?string
    {
        return $this->ctPrixttc;
    }

    public function setCtPrixttc(?string $ctPrixttc): self
    {
        $this->ctPrixttc = $ctPrixttc;

        return $this;
    }


}
