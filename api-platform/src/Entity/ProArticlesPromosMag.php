<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiProperty;

/**
 * ProArticlesPromosMag
 *
 * @ORM\Table(name="pro_articles_promos_mag")
 * @ORM\Entity
 * @ApiResource()
 */
class ProArticlesPromosMag
{
    /**
     * @var string
     *
     * @ORM\Column(name="GUID", type="string", length=100, nullable=false, options={"default":""})
     * @ORM\Id
     * @ApiProperty(identifier=true)
     */
    private $guid;

    /**
     * @var string
     *
     * @ORM\Column(name="AR_REF", type="string", length=18, nullable=false, options={"default":""})
     */
    private $arRef = '';

    /**
     * @var string
     *
     * @ORM\Column(name="FA_CODEFAMILLE", type="string", length=10, nullable=false, options={"default":""})
     */
    private $faCodefamille = '';

    /**
     * @var int
     *
     * @ORM\Column(name="AG_NO1", type="integer", length=6, nullable=false, options={"default":0})
     */
    private $agNo1 = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="AG_NO2", type="integer", length=6, nullable=false, options={"default":0})
     */
    private $agNo2 = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="PrixHT", type="float", precision=10, scale=0, nullable=false, options={"default":0})
     */
    private $prixht = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="PrixTTC", type="float", precision=10, scale=0, nullable=false, options={"default":0})
     */
    private $prixttc = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Date_debut", type="date", nullable=false, options={"default"="0000-00-00"})
     */
    private $dateDebut = '0000-00-00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Date_fin", type="date", nullable=false, options={"default"="0000-00-00"})
     */
    private $dateFin = '0000-00-00';

    public function getGuid(): ?string
    {
        return $this->guid;
    }

    public function setGuid(string $guid): self
    {
        $this->guid = $guid;

        return $this;
    }

    public function getArRef(): ?string
    {
        return $this->arRef;
    }

    public function setArRef(string $arRef): self
    {
        $this->arRef = $arRef;

        return $this;
    }

    public function getFaCodefamille(): ?string
    {
        return $this->faCodefamille;
    }

    public function setFaCodefamille(string $faCodefamille): self
    {
        $this->faCodefamille = $faCodefamille;

        return $this;
    }

    public function getAgNo1(): ?int
    {
        return $this->agNo1;
    }

    public function setAgNo1(int $agNo1): self
    {
        $this->agNo1 = $agNo1;

        return $this;
    }

    public function getAgNo2(): ?int
    {
        return $this->agNo2;
    }

    public function setAgNo2(int $agNo2): self
    {
        $this->agNo2 = $agNo2;

        return $this;
    }

    public function getPrixht(): ?float
    {
        return $this->prixht;
    }

    public function setPrixht(float $prixht): self
    {
        $this->prixht = $prixht;

        return $this;
    }

    public function getPrixttc(): ?float
    {
        return $this->prixttc;
    }

    public function setPrixttc(float $prixttc): self
    {
        $this->prixttc = $prixttc;

        return $this;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->dateDebut;
    }

    public function setDateDebut(\DateTimeInterface $dateDebut): self
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->dateFin;
    }

    public function setDateFin(\DateTimeInterface $dateFin): self
    {
        $this->dateFin = $dateFin;

        return $this;
    }


}
