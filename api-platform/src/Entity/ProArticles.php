<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiProperty;

/**
 * ProArticles
 *
 * @ORM\Table(name="pro_articles", indexes={@ORM\Index(name="Article", columns={"AR_REF", "AG_NO1", "AG_NO2"}), @ORM\Index(name="recherche", columns={"AR_REF", "AR_DESIGN", "FA_INTITULE", "AR_STAT02", "F_ARTGAMME_G1_EG_ENUMERE", "F_ARTGAMME_G2_EG_ENUMERE", "RefUniqueCLV", "RefUnique", "Ref_Four", "CodeBarre"})})
 * @ORM\Entity
 * @ApiResource()
 */
class ProArticles
{
    /**
     * @var string
     *
     * @ORM\Column(name="GUID", type="string", length=100, nullable=false, options={"default":""})
     * @ORM\Id
     * @ApiProperty(identifier=true)
     */
    private $guid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="AR_REF", type="string", length=18, nullable=true)
     */
    private $arRef;

    /**
     * @var string|null
     *
     * @ORM\Column(name="AR_DESIGN", type="string", length=100, nullable=true)
     */
    private $arDesign;

    /**
     * @var string|null
     *
     * @ORM\Column(name="AR_LANGUE1", type="string", length=100, nullable=true)
     */
    private $arLangue1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="AR_LANGUE2", type="string", length=100, nullable=true)
     */
    private $arLangue2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="FA_CODEFAMILLE", type="string", length=10, nullable=true)
     */
    private $faCodefamille;

    /**
     * @var string|null
     *
     * @ORM\Column(name="FA_INTITULE", type="string", length=30, nullable=true)
     */
    private $faIntitule;

    /**
     * @var string
     *
     * @ORM\Column(name="AR_STAT02", type="string", length=100, nullable=false, options={"default":""})
     */
    private $arStat02 = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="AR_GAMME1", type="string", length=4, nullable=true)
     */
    private $arGamme1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="INT_GAMME1", type="string", length=24, nullable=true)
     */
    private $intGamme1;

    /**
     * @var int|null
     *
     * @ORM\Column(name="AG_NO1", type="integer", nullable=true)
     */
    private $agNo1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="F_ARTGAMME_G1_EG_ENUMERE", type="string", length=35, nullable=true)
     */
    private $fArtgammeG1EgEnumere;

    /**
     * @var string|null
     *
     * @ORM\Column(name="AR_GAMME2", type="string", length=4, nullable=true)
     */
    private $arGamme2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="INT_GAMME2", type="string", length=23, nullable=true)
     */
    private $intGamme2;

    /**
     * @var int|null
     *
     * @ORM\Column(name="AG_NO2", type="integer", nullable=true)
     */
    private $agNo2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="F_ARTGAMME_G2_EG_ENUMERE", type="string", length=35, nullable=true)
     */
    private $fArtgammeG2EgEnumere;

    /**
     * @var string|null
     *
     * @ORM\Column(name="AR_PRIXACH", type="string", length=6, nullable=true)
     */
    private $arPrixach;

    /**
     * @var string|null
     *
     * @ORM\Column(name="AR_PRIXVEN", type="string", length=6, nullable=true)
     */
    private $arPrixven;

    /**
     * @var string|null
     *
     * @ORM\Column(name="AR_PHOTO", type="string", length=37, nullable=true)
     */
    private $arPhoto;

    /**
     * @var int|null
     *
     * @ORM\Column(name="StockReel", type="integer", nullable=true)
     */
    private $stockreel;

    /**
     * @var int|null
     *
     * @ORM\Column(name="Stock_A_Terme", type="integer", nullable=true)
     */
    private $stockATerme;

    /**
     * @var string
     *
     * @ORM\Column(name="DO_DATELIVR_CMD_FOURNISSEUR", type="string", length=20, nullable=false, options={"default":""})
     */
    private $doDatelivrCmdFournisseur = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="DO_STATUT_CMD_FOURNISSEUR", type="string", length=20, nullable=true)
     */
    private $doStatutCmdFournisseur;

    /**
     * @var string|null
     *
     * @ORM\Column(name="DO_TYPE_CMD_FOURNISSEUR", type="string", length=20, nullable=true)
     */
    private $doTypeCmdFournisseur;

    /**
     * @var string
     *
     * @ORM\Column(name="AR_TYPE", type="string", length=15, nullable=false, options={"default":""})
     */
    private $arType = '';

    /**
     * @var int
     *
     * @ORM\Column(name="DO_Publie", type="integer", nullable=false, options={"default":0})
     */
    private $doPublie = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="RefUniqueCLV", type="string", length=28, nullable=false, options={"default":""})
     */
    private $refuniqueclv = '';

    /**
     * @var string
     *
     * @ORM\Column(name="RefUnique", type="string", length=28, nullable=false, options={"default":""})
     */
    private $refunique = '';

    /**
     * @var string
     *
     * @ORM\Column(name="NOS", type="string", length=10, nullable=false, options={"default":""})
     */
    private $nos = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="NOUVEAUTE", type="string", length=20, nullable=true)
     */
    private $nouveaute;

    /**
     * @var int
     *
     * @ORM\Column(name="AR_Nomecl", type="integer", nullable=false, options={"default":0})
     */
    private $arNomecl = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="CL_No1", type="integer", nullable=false, options={"default":0})
     */
    private $clNo1 = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="CL_No1_Intitule", type="string", length=40, nullable=false, options={"default":0})
     */
    private $clNo1Intitule = '';

    /**
     * @var int
     *
     * @ORM\Column(name="CL_No2", type="integer", nullable=false, options={"default":0})
     */
    private $clNo2 = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="CL_No2_Intitule", type="string", length=40, nullable=false, options={"default":0})
     */
    private $clNo2Intitule = '';

    /**
     * @var string
     *
     * @ORM\Column(name="Ref_Four", type="string", length=20, nullable=false, options={"default":""})
     */
    private $refFour = '';

    /**
     * @var string
     *
     * @ORM\Column(name="CodeBarre", type="string", length=20, nullable=false, options={"default":""})
     */
    private $codebarre = '';

    public function getGuid(): ?string
    {
        return $this->guid;
    }

    public function setGuid(string $guid): self
    {
        $this->guid = $guid;

        return $this;
    }

    public function getArRef(): ?string
    {
        return $this->arRef;
    }

    public function setArRef(?string $arRef): self
    {
        $this->arRef = $arRef;

        return $this;
    }

    public function getArDesign(): ?string
    {
        return $this->arDesign;
    }

    public function setArDesign(?string $arDesign): self
    {
        $this->arDesign = $arDesign;

        return $this;
    }

    public function getArLangue1(): ?string
    {
        return $this->arLangue1;
    }

    public function setArLangue1(?string $arLangue1): self
    {
        $this->arLangue1 = $arLangue1;

        return $this;
    }

    public function getArLangue2(): ?string
    {
        return $this->arLangue2;
    }

    public function setArLangue2(?string $arLangue2): self
    {
        $this->arLangue2 = $arLangue2;

        return $this;
    }

    public function getFaCodefamille(): ?string
    {
        return $this->faCodefamille;
    }

    public function setFaCodefamille(?string $faCodefamille): self
    {
        $this->faCodefamille = $faCodefamille;

        return $this;
    }

    public function getFaIntitule(): ?string
    {
        return $this->faIntitule;
    }

    public function setFaIntitule(?string $faIntitule): self
    {
        $this->faIntitule = $faIntitule;

        return $this;
    }

    public function getArStat02(): ?string
    {
        return $this->arStat02;
    }

    public function setArStat02(string $arStat02): self
    {
        $this->arStat02 = $arStat02;

        return $this;
    }

    public function getArGamme1(): ?string
    {
        return $this->arGamme1;
    }

    public function setArGamme1(?string $arGamme1): self
    {
        $this->arGamme1 = $arGamme1;

        return $this;
    }

    public function getIntGamme1(): ?string
    {
        return $this->intGamme1;
    }

    public function setIntGamme1(?string $intGamme1): self
    {
        $this->intGamme1 = $intGamme1;

        return $this;
    }

    public function getAgNo1(): ?int
    {
        return $this->agNo1;
    }

    public function setAgNo1(?int $agNo1): self
    {
        $this->agNo1 = $agNo1;

        return $this;
    }

    public function getFArtgammeG1EgEnumere(): ?string
    {
        return $this->fArtgammeG1EgEnumere;
    }

    public function setFArtgammeG1EgEnumere(?string $fArtgammeG1EgEnumere): self
    {
        $this->fArtgammeG1EgEnumere = $fArtgammeG1EgEnumere;

        return $this;
    }

    public function getArGamme2(): ?string
    {
        return $this->arGamme2;
    }

    public function setArGamme2(?string $arGamme2): self
    {
        $this->arGamme2 = $arGamme2;

        return $this;
    }

    public function getIntGamme2(): ?string
    {
        return $this->intGamme2;
    }

    public function setIntGamme2(?string $intGamme2): self
    {
        $this->intGamme2 = $intGamme2;

        return $this;
    }

    public function getAgNo2(): ?int
    {
        return $this->agNo2;
    }

    public function setAgNo2(?int $agNo2): self
    {
        $this->agNo2 = $agNo2;

        return $this;
    }

    public function getFArtgammeG2EgEnumere(): ?string
    {
        return $this->fArtgammeG2EgEnumere;
    }

    public function setFArtgammeG2EgEnumere(?string $fArtgammeG2EgEnumere): self
    {
        $this->fArtgammeG2EgEnumere = $fArtgammeG2EgEnumere;

        return $this;
    }

    public function getArPrixach(): ?string
    {
        return $this->arPrixach;
    }

    public function setArPrixach(?string $arPrixach): self
    {
        $this->arPrixach = $arPrixach;

        return $this;
    }

    public function getArPrixven(): ?string
    {
        return $this->arPrixven;
    }

    public function setArPrixven(?string $arPrixven): self
    {
        $this->arPrixven = $arPrixven;

        return $this;
    }

    public function getArPhoto(): ?string
    {
        return $this->arPhoto;
    }

    public function setArPhoto(?string $arPhoto): self
    {
        $this->arPhoto = $arPhoto;

        return $this;
    }

    public function getStockreel(): ?int
    {
        return $this->stockreel;
    }

    public function setStockreel(?int $stockreel): self
    {
        $this->stockreel = $stockreel;

        return $this;
    }

    public function getStockATerme(): ?int
    {
        return $this->stockATerme;
    }

    public function setStockATerme(?int $stockATerme): self
    {
        $this->stockATerme = $stockATerme;

        return $this;
    }

    public function getDoDatelivrCmdFournisseur(): ?string
    {
        return $this->doDatelivrCmdFournisseur;
    }

    public function setDoDatelivrCmdFournisseur(string $doDatelivrCmdFournisseur): self
    {
        $this->doDatelivrCmdFournisseur = $doDatelivrCmdFournisseur;

        return $this;
    }

    public function getDoStatutCmdFournisseur(): ?string
    {
        return $this->doStatutCmdFournisseur;
    }

    public function setDoStatutCmdFournisseur(?string $doStatutCmdFournisseur): self
    {
        $this->doStatutCmdFournisseur = $doStatutCmdFournisseur;

        return $this;
    }

    public function getDoTypeCmdFournisseur(): ?string
    {
        return $this->doTypeCmdFournisseur;
    }

    public function setDoTypeCmdFournisseur(?string $doTypeCmdFournisseur): self
    {
        $this->doTypeCmdFournisseur = $doTypeCmdFournisseur;

        return $this;
    }

    public function getArType(): ?string
    {
        return $this->arType;
    }

    public function setArType(string $arType): self
    {
        $this->arType = $arType;

        return $this;
    }

    public function getDoPublie(): ?int
    {
        return $this->doPublie;
    }

    public function setDoPublie(int $doPublie): self
    {
        $this->doPublie = $doPublie;

        return $this;
    }

    public function getRefuniqueclv(): ?string
    {
        return $this->refuniqueclv;
    }

    public function setRefuniqueclv(string $refuniqueclv): self
    {
        $this->refuniqueclv = $refuniqueclv;

        return $this;
    }

    public function getRefunique(): ?string
    {
        return $this->refunique;
    }

    public function setRefunique(string $refunique): self
    {
        $this->refunique = $refunique;

        return $this;
    }

    public function getNos(): ?string
    {
        return $this->nos;
    }

    public function setNos(string $nos): self
    {
        $this->nos = $nos;

        return $this;
    }

    public function getNouveaute(): ?string
    {
        return $this->nouveaute;
    }

    public function setNouveaute(?string $nouveaute): self
    {
        $this->nouveaute = $nouveaute;

        return $this;
    }

    public function getArNomecl(): ?int
    {
        return $this->arNomecl;
    }

    public function setArNomecl(int $arNomecl): self
    {
        $this->arNomecl = $arNomecl;

        return $this;
    }

    public function getClNo1(): ?int
    {
        return $this->clNo1;
    }

    public function setClNo1(int $clNo1): self
    {
        $this->clNo1 = $clNo1;

        return $this;
    }

    public function getClNo1Intitule(): ?string
    {
        return $this->clNo1Intitule;
    }

    public function setClNo1Intitule(string $clNo1Intitule): self
    {
        $this->clNo1Intitule = $clNo1Intitule;

        return $this;
    }

    public function getClNo2(): ?int
    {
        return $this->clNo2;
    }

    public function setClNo2(int $clNo2): self
    {
        $this->clNo2 = $clNo2;

        return $this;
    }

    public function getClNo2Intitule(): ?string
    {
        return $this->clNo2Intitule;
    }

    public function setClNo2Intitule(string $clNo2Intitule): self
    {
        $this->clNo2Intitule = $clNo2Intitule;

        return $this;
    }

    public function getRefFour(): ?string
    {
        return $this->refFour;
    }

    public function setRefFour(string $refFour): self
    {
        $this->refFour = $refFour;

        return $this;
    }

    public function getCodebarre(): ?string
    {
        return $this->codebarre;
    }

    public function setCodebarre(string $codebarre): self
    {
        $this->codebarre = $codebarre;

        return $this;
    }


}
