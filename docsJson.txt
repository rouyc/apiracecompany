{
openapi: "3.0.2",
servers: [
{
url: "/api/api-platform/public"
}
],
info: {
title: "",
version: "0.0.0"
},
paths: {
/api/pro_clients: {
get: {
tags: [
"ProClients"
],
operationId: "getProClientsCollection",
summary: "Retrieves the collection of ProClients resources.",
responses: {
200: {
description: "ProClients collection response",
content: {
application/json: {
schema: {
type: "array",
items: {
$ref: "#/components/schemas/ProClients"
}
}
}
}
}
},
parameters: [
{
name: "page",
in: "query",
required: false,
description: "The collection page number",
schema: {
type: "integer",
default: 1
}
}
]
},
post: {
tags: [
"ProClients"
],
operationId: "postProClientsCollection",
summary: "Creates a ProClients resource.",
responses: {
201: {
description: "ProClients resource created",
content: {
application/json: {
schema: {
$ref: "#/components/schemas/ProClients"
}
}
},
links: {
GetProClientsItem: {
parameters: {
ctNum: "$response.body#/ctNum"
},
operationId: "getProClientsItem",
description: "The `ctNum` value returned in the response can be used as the `ctNum` parameter in `GET /api/pro_clients/{id}`."
}
}
},
400: {
description: "Invalid input"
},
404: {
description: "Resource not found"
}
},
requestBody: {
content: {
application/json: {
schema: {
$ref: "#/components/schemas/ProClients"
}
}
},
description: "The new ProClients resource"
}
}
},
/api/pro_clients/{id}: {
get: {
tags: [
"ProClients"
],
operationId: "getProClientsItem",
summary: "Retrieves a ProClients resource.",
parameters: [
{
name: "id",
in: "path",
required: true,
schema: {
type: "string"
}
}
],
responses: {
200: {
description: "ProClients resource response",
content: {
application/json: {
schema: {
$ref: "#/components/schemas/ProClients"
}
}
}
},
404: {
description: "Resource not found"
}
}
},
delete: {
tags: [
"ProClients"
],
operationId: "deleteProClientsItem",
summary: "Removes the ProClients resource.",
responses: {
204: {
description: "ProClients resource deleted"
},
404: {
description: "Resource not found"
}
},
parameters: [
{
name: "id",
in: "path",
required: true,
schema: {
type: "string"
}
}
]
},
put: {
tags: [
"ProClients"
],
operationId: "putProClientsItem",
summary: "Replaces the ProClients resource.",
parameters: [
{
name: "id",
in: "path",
required: true,
schema: {
type: "string"
}
}
],
responses: {
200: {
description: "ProClients resource updated",
content: {
application/json: {
schema: {
$ref: "#/components/schemas/ProClients"
}
}
}
},
400: {
description: "Invalid input"
},
404: {
description: "Resource not found"
}
},
requestBody: {
content: {
application/json: {
schema: {
$ref: "#/components/schemas/ProClients"
}
}
},
description: "The updated ProClients resource"
}
},
patch: {
tags: [
"ProClients"
],
operationId: "patchProClientsItem",
summary: "Updates the ProClients resource.",
parameters: [
{
name: "id",
in: "path",
required: true,
schema: {
type: "string"
}
}
],
responses: {
200: {
description: "ProClients resource updated",
content: {
application/json: {
schema: {
$ref: "#/components/schemas/ProClients"
}
}
}
},
400: {
description: "Invalid input"
},
404: {
description: "Resource not found"
}
},
requestBody: {
content: {
application/merge-patch+json: {
schema: {
$ref: "#/components/schemas/ProClients"
}
}
},
description: "The updated ProClients resource"
}
}
},
/api/pro_clients_geocodes: {
get: {
tags: [
"ProClientsGeocode"
],
operationId: "getProClientsGeocodeCollection",
summary: "Retrieves the collection of ProClientsGeocode resources.",
responses: {
200: {
description: "ProClientsGeocode collection response",
content: {
application/json: {
schema: {
type: "array",
items: {
$ref: "#/components/schemas/ProClientsGeocode"
}
}
}
}
}
},
parameters: [
{
name: "page",
in: "query",
required: false,
description: "The collection page number",
schema: {
type: "integer",
default: 1
}
}
]
},
post: {
tags: [
"ProClientsGeocode"
],
operationId: "postProClientsGeocodeCollection",
summary: "Creates a ProClientsGeocode resource.",
responses: {
201: {
description: "ProClientsGeocode resource created",
content: {
application/json: {
schema: {
$ref: "#/components/schemas/ProClientsGeocode"
}
}
},
links: {
GetProClientsGeocodeItem: {
parameters: {
ctNum: "$response.body#/ctNum"
},
operationId: "getProClientsGeocodeItem",
description: "The `ctNum` value returned in the response can be used as the `ctNum` parameter in `GET /api/pro_clients_geocodes/{id}`."
}
}
},
400: {
description: "Invalid input"
},
404: {
description: "Resource not found"
}
},
requestBody: {
content: {
application/json: {
schema: {
$ref: "#/components/schemas/ProClientsGeocode"
}
}
},
description: "The new ProClientsGeocode resource"
}
}
},
/api/pro_clients_geocodes/{id}: {
get: {
tags: [
"ProClientsGeocode"
],
operationId: "getProClientsGeocodeItem",
summary: "Retrieves a ProClientsGeocode resource.",
parameters: [
{
name: "id",
in: "path",
required: true,
schema: {
type: "string"
}
}
],
responses: {
200: {
description: "ProClientsGeocode resource response",
content: {
application/json: {
schema: {
$ref: "#/components/schemas/ProClientsGeocode"
}
}
}
},
404: {
description: "Resource not found"
}
}
},
delete: {
tags: [
"ProClientsGeocode"
],
operationId: "deleteProClientsGeocodeItem",
summary: "Removes the ProClientsGeocode resource.",
responses: {
204: {
description: "ProClientsGeocode resource deleted"
},
404: {
description: "Resource not found"
}
},
parameters: [
{
name: "id",
in: "path",
required: true,
schema: {
type: "string"
}
}
]
},
put: {
tags: [
"ProClientsGeocode"
],
operationId: "putProClientsGeocodeItem",
summary: "Replaces the ProClientsGeocode resource.",
parameters: [
{
name: "id",
in: "path",
required: true,
schema: {
type: "string"
}
}
],
responses: {
200: {
description: "ProClientsGeocode resource updated",
content: {
application/json: {
schema: {
$ref: "#/components/schemas/ProClientsGeocode"
}
}
}
},
400: {
description: "Invalid input"
},
404: {
description: "Resource not found"
}
},
requestBody: {
content: {
application/json: {
schema: {
$ref: "#/components/schemas/ProClientsGeocode"
}
}
},
description: "The updated ProClientsGeocode resource"
}
},
patch: {
tags: [
"ProClientsGeocode"
],
operationId: "patchProClientsGeocodeItem",
summary: "Updates the ProClientsGeocode resource.",
parameters: [
{
name: "id",
in: "path",
required: true,
schema: {
type: "string"
}
}
],
responses: {
200: {
description: "ProClientsGeocode resource updated",
content: {
application/json: {
schema: {
$ref: "#/components/schemas/ProClientsGeocode"
}
}
}
},
400: {
description: "Invalid input"
},
404: {
description: "Resource not found"
}
},
requestBody: {
content: {
application/merge-patch+json: {
schema: {
$ref: "#/components/schemas/ProClientsGeocode"
}
}
},
description: "The updated ProClientsGeocode resource"
}
}
},
/api/pro_clients_lieux_livraisons: {
get: {
tags: [
"ProClientsLieuxLivraison"
],
operationId: "getProClientsLieuxLivraisonCollection",
summary: "Retrieves the collection of ProClientsLieuxLivraison resources.",
responses: {
200: {
description: "ProClientsLieuxLivraison collection response",
content: {
application/json: {
schema: {
type: "array",
items: {
$ref: "#/components/schemas/ProClientsLieuxLivraison"
}
}
}
}
}
},
parameters: [
{
name: "page",
in: "query",
required: false,
description: "The collection page number",
schema: {
type: "integer",
default: 1
}
}
]
},
post: {
tags: [
"ProClientsLieuxLivraison"
],
operationId: "postProClientsLieuxLivraisonCollection",
summary: "Creates a ProClientsLieuxLivraison resource.",
responses: {
201: {
description: "ProClientsLieuxLivraison resource created",
content: {
application/json: {
schema: {
$ref: "#/components/schemas/ProClientsLieuxLivraison"
}
}
},
links: {
GetProClientsLieuxLivraisonItem: {
parameters: {
liNo: "$response.body#/liNo"
},
operationId: "getProClientsLieuxLivraisonItem",
description: "The `liNo` value returned in the response can be used as the `liNo` parameter in `GET /api/pro_clients_lieux_livraisons/{id}`."
}
}
},
400: {
description: "Invalid input"
},
404: {
description: "Resource not found"
}
},
requestBody: {
content: {
application/json: {
schema: {
$ref: "#/components/schemas/ProClientsLieuxLivraison"
}
}
},
description: "The new ProClientsLieuxLivraison resource"
}
}
},
/api/pro_clients_lieux_livraisons/{id}: {
get: {
tags: [
"ProClientsLieuxLivraison"
],
operationId: "getProClientsLieuxLivraisonItem",
summary: "Retrieves a ProClientsLieuxLivraison resource.",
parameters: [
{
name: "id",
in: "path",
required: true,
schema: {
type: "string"
}
}
],
responses: {
200: {
description: "ProClientsLieuxLivraison resource response",
content: {
application/json: {
schema: {
$ref: "#/components/schemas/ProClientsLieuxLivraison"
}
}
}
},
404: {
description: "Resource not found"
}
}
},
delete: {
tags: [
"ProClientsLieuxLivraison"
],
operationId: "deleteProClientsLieuxLivraisonItem",
summary: "Removes the ProClientsLieuxLivraison resource.",
responses: {
204: {
description: "ProClientsLieuxLivraison resource deleted"
},
404: {
description: "Resource not found"
}
},
parameters: [
{
name: "id",
in: "path",
required: true,
schema: {
type: "string"
}
}
]
},
put: {
tags: [
"ProClientsLieuxLivraison"
],
operationId: "putProClientsLieuxLivraisonItem",
summary: "Replaces the ProClientsLieuxLivraison resource.",
parameters: [
{
name: "id",
in: "path",
required: true,
schema: {
type: "string"
}
}
],
responses: {
200: {
description: "ProClientsLieuxLivraison resource updated",
content: {
application/json: {
schema: {
$ref: "#/components/schemas/ProClientsLieuxLivraison"
}
}
}
},
400: {
description: "Invalid input"
},
404: {
description: "Resource not found"
}
},
requestBody: {
content: {
application/json: {
schema: {
$ref: "#/components/schemas/ProClientsLieuxLivraison"
}
}
},
description: "The updated ProClientsLieuxLivraison resource"
}
},
patch: {
tags: [
"ProClientsLieuxLivraison"
],
operationId: "patchProClientsLieuxLivraisonItem",
summary: "Updates the ProClientsLieuxLivraison resource.",
parameters: [
{
name: "id",
in: "path",
required: true,
schema: {
type: "string"
}
}
],
responses: {
200: {
description: "ProClientsLieuxLivraison resource updated",
content: {
application/json: {
schema: {
$ref: "#/components/schemas/ProClientsLieuxLivraison"
}
}
}
},
400: {
description: "Invalid input"
},
404: {
description: "Resource not found"
}
},
requestBody: {
content: {
application/merge-patch+json: {
schema: {
$ref: "#/components/schemas/ProClientsLieuxLivraison"
}
}
},
description: "The updated ProClientsLieuxLivraison resource"
}
}
},
/api/pro_clients_marques: {
get: {
tags: [
"ProClientsMarques"
],
operationId: "getProClientsMarquesCollection",
summary: "Retrieves the collection of ProClientsMarques resources.",
responses: {
200: {
description: "ProClientsMarques collection response",
content: {
application/json: {
schema: {
type: "array",
items: {
$ref: "#/components/schemas/ProClientsMarques"
}
}
}
}
}
},
parameters: [
{
name: "page",
in: "query",
required: false,
description: "The collection page number",
schema: {
type: "integer",
default: 1
}
}
]
},
post: {
tags: [
"ProClientsMarques"
],
operationId: "postProClientsMarquesCollection",
summary: "Creates a ProClientsMarques resource.",
responses: {
201: {
description: "ProClientsMarques resource created",
content: {
application/json: {
schema: {
$ref: "#/components/schemas/ProClientsMarques"
}
}
},
links: {
GetProClientsMarquesItem: {
parameters: {
id: "$response.body#/id"
},
operationId: "getProClientsMarquesItem",
description: "The `id` value returned in the response can be used as the `id` parameter in `GET /api/pro_clients_marques/{id}`."
}
}
},
400: {
description: "Invalid input"
},
404: {
description: "Resource not found"
}
},
requestBody: {
content: {
application/json: {
schema: {
$ref: "#/components/schemas/ProClientsMarques"
}
}
},
description: "The new ProClientsMarques resource"
}
}
},
/api/pro_clients_marques/{id}: {
get: {
tags: [
"ProClientsMarques"
],
operationId: "getProClientsMarquesItem",
summary: "Retrieves a ProClientsMarques resource.",
parameters: [
{
name: "id",
in: "path",
required: true,
schema: {
type: "string"
}
}
],
responses: {
200: {
description: "ProClientsMarques resource response",
content: {
application/json: {
schema: {
$ref: "#/components/schemas/ProClientsMarques"
}
}
}
},
404: {
description: "Resource not found"
}
}
},
delete: {
tags: [
"ProClientsMarques"
],
operationId: "deleteProClientsMarquesItem",
summary: "Removes the ProClientsMarques resource.",
responses: {
204: {
description: "ProClientsMarques resource deleted"
},
404: {
description: "Resource not found"
}
},
parameters: [
{
name: "id",
in: "path",
required: true,
schema: {
type: "string"
}
}
]
},
put: {
tags: [
"ProClientsMarques"
],
operationId: "putProClientsMarquesItem",
summary: "Replaces the ProClientsMarques resource.",
parameters: [
{
name: "id",
in: "path",
required: true,
schema: {
type: "string"
}
}
],
responses: {
200: {
description: "ProClientsMarques resource updated",
content: {
application/json: {
schema: {
$ref: "#/components/schemas/ProClientsMarques"
}
}
}
},
400: {
description: "Invalid input"
},
404: {
description: "Resource not found"
}
},
requestBody: {
content: {
application/json: {
schema: {
$ref: "#/components/schemas/ProClientsMarques"
}
}
},
description: "The updated ProClientsMarques resource"
}
},
patch: {
tags: [
"ProClientsMarques"
],
operationId: "patchProClientsMarquesItem",
summary: "Updates the ProClientsMarques resource.",
parameters: [
{
name: "id",
in: "path",
required: true,
schema: {
type: "string"
}
}
],
responses: {
200: {
description: "ProClientsMarques resource updated",
content: {
application/json: {
schema: {
$ref: "#/components/schemas/ProClientsMarques"
}
}
}
},
400: {
description: "Invalid input"
},
404: {
description: "Resource not found"
}
},
requestBody: {
content: {
application/merge-patch+json: {
schema: {
$ref: "#/components/schemas/ProClientsMarques"
}
}
},
description: "The updated ProClientsMarques resource"
}
}
}
},
components: {
schemas: {
ProClients: {
type: "object",
description: "ProClients",
properties: {
ctNum: {
type: "string"
},
ctIntitule: {
type: "string",
nullable: true
},
ctClassement: {
type: "string",
nullable: true
},
ctAdresse: {
type: "string",
nullable: true
},
ctComplement: {
type: "string",
nullable: true
},
ctCodepostal: {
type: "string",
nullable: true
},
ctVille: {
type: "string",
nullable: true
},
ctCoderegion: {
type: "string",
nullable: true
},
ctPays: {
type: "string",
nullable: true
},
ctTelephone: {
type: "string"
},
ctTelecopie: {
type: "string"
},
ctEmail: {
type: "string"
},
ctSite: {
type: "string"
},
reNo: {
type: "string",
nullable: true
},
nCattarif: {
type: "integer",
nullable: true
},
tauxremise: {
type: "string",
nullable: true
},
tauxescompte: {
type: "string",
nullable: true
},
tva: {
type: "string"
},
mrIntitule: {
type: "string"
},
passwordB2b: {
type: "string"
},
newsletter: {
type: "string"
},
commercial: {
type: "string"
},
ctType: {
type: "string"
}
}
},
ProClientsGeocode: {
type: "object",
description: "ProClientsGeocode",
properties: {
ctNum: {
type: "string"
},
lat: {
type: "string"
},
lng: {
type: "string"
},
locationtype: {
type: "string"
}
}
},
ProClientsLieuxLivraison: {
type: "object",
description: "ProClientsLieuxLivraison",
properties: {
liNo: {
type: "string"
},
ctNum: {
type: "string"
},
liIntitule: {
type: "string"
},
liAdresse: {
type: "string"
},
liComplement: {
type: "string"
},
liCodepostal: {
type: "string"
},
liVille: {
type: "string"
},
liCoderegion: {
type: "string"
},
liPays: {
type: "string"
},
liPrincipal: {
type: "integer"
}
}
},
ProClientsMarques: {
type: "object",
description: "",
properties: {
id: {
readOnly: true,
type: "integer"
},
ctNum: {
type: "string"
},
marque: {
type: "string"
}
}
}
}
}
}