<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiProperty;

/**
 * ProClientsGeocode
 *
 * @ORM\Table(name="pro_clients_geocode")
 * @ORM\Entity
 * @ApiResource()
 */
class ProClientsGeocode
{
    /**
     * @var string
     *
     * @ORM\Column(name="CT_NUM", type="string", length=30, nullable=false, options={"default":""})
     * @ORM\Id
     * @ApiProperty(identifier=true)
     */
    private $ctNum = '';

    /**
     * @var string
     *
     * @ORM\Column(name="lat", type="string", length=12, nullable=false, options={"default":0})
     */
    private $lat = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="lng", type="string", length=12, nullable=false, options={"default":0})
     */
    private $lng = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="locationType", type="string", length=30, nullable=false)
     */
    private $locationtype;

    public function getCtNum(): ?string
    {
        return $this->ctNum;
    }

    public function setCtNum(string $ctNum): self
    {
        $this->ctNum = $ctNum;

        return $this;
    }

    public function getLat(): ?string
    {
        return $this->lat;
    }

    public function setLat(string $lat): self
    {
        $this->lat = $lat;

        return $this;
    }

    public function getLng(): ?string
    {
        return $this->lng;
    }

    public function setLng(string $lng): self
    {
        $this->lng = $lng;

        return $this;
    }

    public function getLocationtype(): ?string
    {
        return $this->locationtype;
    }

    public function setLocationtype(string $locationtype): self
    {
        $this->locationtype = $locationtype;

        return $this;
    }


}
