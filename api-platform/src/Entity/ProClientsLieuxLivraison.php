<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiProperty;

/**
 * ProClientsLieuxLivraison
 *
 * @ORM\Table(name="pro_clients_lieux_livraison", uniqueConstraints={@ORM\UniqueConstraint(name="LI_NO", columns={"LI_NO"})})
 * @ORM\Entity
 * @ApiResource()
 */
class ProClientsLieuxLivraison
{
    /**
     * @var string
     *
     * @ORM\Column(name="LI_NO", type="string", length=9, nullable=false, options={"default":""})
     * @ORM\Id
     * @ApiProperty(identifier=true)
     */
    private $liNo = '';

    /**
     * @var string
     *
     * @ORM\Column(name="CT_NUM", type="string", length=30, nullable=false, options={"default":""})
     */
    private $ctNum = '';

    /**
     * @var string
     *
     * @ORM\Column(name="LI_INTITULE", type="string", length=100, nullable=false, options={"default":""})
     */
    private $liIntitule = '';

    /**
     * @var string
     *
     * @ORM\Column(name="LI_ADRESSE", type="text", length=255, nullable=false, options={"default":""})
     */
    private $liAdresse;

    /**
     * @var string
     *
     * @ORM\Column(name="LI_COMPLEMENT", type="text", length=255, nullable=false, options={"default":""})
     */
    private $liComplement;

    /**
     * @var string
     *
     * @ORM\Column(name="LI_CODEPOSTAL", type="string", length=10, nullable=false, options={"default":""})
     */
    private $liCodepostal = '';

    /**
     * @var string
     *
     * @ORM\Column(name="LI_VILLE", type="string", length=50, nullable=false, options={"default":""})
     */
    private $liVille = '';

    /**
     * @var string
     *
     * @ORM\Column(name="LI_CODEREGION", type="string", length=50, nullable=false, options={"default":""})
     */
    private $liCoderegion = '';

    /**
     * @var string
     *
     * @ORM\Column(name="LI_PAYS", type="string", length=50, nullable=false, options={"default":""})
     */
    private $liPays = '';

    /**
     * @var int
     *
     * @ORM\Column(name="LI_PRINCIPAL", type="integer", nullable=false, options={"default":0})
     */
    private $liPrincipal = '0';

    public function getLiNo(): ?string
    {
        return $this->liNo;
    }

    public function setLiNo(string $liNo): self
    {
        $this->liNo = $liNo;

        return $this;
    }

    public function getCtNum(): ?string
    {
        return $this->ctNum;
    }

    public function setCtNum(string $ctNum): self
    {
        $this->ctNum = $ctNum;

        return $this;
    }

    public function getLiIntitule(): ?string
    {
        return $this->liIntitule;
    }

    public function setLiIntitule(string $liIntitule): self
    {
        $this->liIntitule = $liIntitule;

        return $this;
    }

    public function getLiAdresse(): ?string
    {
        return $this->liAdresse;
    }

    public function setLiAdresse(string $liAdresse): self
    {
        $this->liAdresse = $liAdresse;

        return $this;
    }

    public function getLiComplement(): ?string
    {
        return $this->liComplement;
    }

    public function setLiComplement(string $liComplement): self
    {
        $this->liComplement = $liComplement;

        return $this;
    }

    public function getLiCodepostal(): ?string
    {
        return $this->liCodepostal;
    }

    public function setLiCodepostal(string $liCodepostal): self
    {
        $this->liCodepostal = $liCodepostal;

        return $this;
    }

    public function getLiVille(): ?string
    {
        return $this->liVille;
    }

    public function setLiVille(string $liVille): self
    {
        $this->liVille = $liVille;

        return $this;
    }

    public function getLiCoderegion(): ?string
    {
        return $this->liCoderegion;
    }

    public function setLiCoderegion(string $liCoderegion): self
    {
        $this->liCoderegion = $liCoderegion;

        return $this;
    }

    public function getLiPays(): ?string
    {
        return $this->liPays;
    }

    public function setLiPays(string $liPays): self
    {
        $this->liPays = $liPays;

        return $this;
    }

    public function getLiPrincipal(): ?int
    {
        return $this->liPrincipal;
    }

    public function setLiPrincipal(int $liPrincipal): self
    {
        $this->liPrincipal = $liPrincipal;

        return $this;
    }


}
