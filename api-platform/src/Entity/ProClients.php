<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiProperty;

/**
 * ProClients
 *
 * @ORM\Table(name="pro_clients", uniqueConstraints={@ORM\UniqueConstraint(name="CT_NUM", columns={"CT_NUM"})})
 * @ORM\Entity
 * @ApiResource()
 */
class ProClients
{
    /**
     * @var string
     *
     * @ORM\Column(name="CT_NUM", type="string", length=30, nullable=false, options={"default":""})
     * @ORM\Id
     * @ApiProperty(identifier=true)
     */
    private $ctNum;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CT_INTITULE", type="string", length=50, nullable=true)
     */
    private $ctIntitule;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CT_CLASSEMENT", type="string", length=50, nullable=true)
     */
    private $ctClassement;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CT_ADRESSE", type="text", length=255, nullable=true)
     */
    private $ctAdresse;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CT_COMPLEMENT", type="text", length=255, nullable=true)
     */
    private $ctComplement;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CT_CODEPOSTAL", type="string", length=8, nullable=true)
     */
    private $ctCodepostal;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CT_VILLE", type="string", length=29, nullable=true)
     */
    private $ctVille;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CT_CODEREGION", type="string", length=30, nullable=true)
     */
    private $ctCoderegion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CT_PAYS", type="string", length=18, nullable=true)
     */
    private $ctPays;

    /**
     * @var string
     *
     * @ORM\Column(name="CT_TELEPHONE", type="string", length=30, nullable=false, options={"default":""})
     */
    private $ctTelephone = '';

    /**
     * @var string
     *
     * @ORM\Column(name="CT_TELECOPIE", type="string", length=30, nullable=false, options={"default":""})
     */
    private $ctTelecopie = '';

    /**
     * @var string
     *
     * @ORM\Column(name="CT_EMAIL", type="string", length=70, nullable=false, options={"default":""})
     */
    private $ctEmail = '';

    /**
     * @var string
     *
     * @ORM\Column(name="CT_SITE", type="string", length=150, nullable=false, options={"default":""})
     */
    private $ctSite = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="RE_NO", type="string", length=2, nullable=true, options={"fixed"=true})
     */
    private $reNo;

    /**
     * @var int|null
     *
     * @ORM\Column(name="N_CATTARIF", type="integer", nullable=true)
     */
    private $nCattarif;

    /**
     * @var string|null
     *
     * @ORM\Column(name="TauxRemise", type="string", length=15, nullable=true)
     */
    private $tauxremise;

    /**
     * @var string|null
     *
     * @ORM\Column(name="TauxEscompte", type="string", length=15, nullable=true)
     */
    private $tauxescompte;

    /**
     * @var string
     *
     * @ORM\Column(name="TVA", type="string", length=4, nullable=false, options={"default":""})
     */
    private $tva = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MR_INTITULE", type="text", length=65535, nullable=false, options={"default":""})
     */
    private $mrIntitule;

    /**
     * @var string
     *
     * @ORM\Column(name="password_B2B", type="string", length=20, nullable=false, options={"default":""})
     */
    private $passwordB2b = '';

    /**
     * @var string
     *
     * @ORM\Column(name="newsletter", type="string", length=21, nullable=false, options={"default":""})
     */
    private $newsletter = '';

    /**
     * @var string
     *
     * @ORM\Column(name="commercial", type="string", length=35, nullable=false, options={"default":""})
     */
    private $commercial = '';

    /**
     * @var string
     *
     * @ORM\Column(name="CT_TYPE", type="string", length=30, nullable=false, options={"default":""})
     */
    private $ctType = '';

    public function getCtNum(): ?string
    {
        return $this->ctNum;
    }

    public function setCtNum(?string $ctNum): self
    {
        $this->ctNum = $ctNum;

        return $this;
    }

    public function getCtIntitule(): ?string
    {
        return $this->ctIntitule;
    }

    public function setCtIntitule(?string $ctIntitule): self
    {
        $this->ctIntitule = $ctIntitule;

        return $this;
    }

    public function getCtClassement(): ?string
    {
        return $this->ctClassement;
    }

    public function setCtClassement(?string $ctClassement): self
    {
        $this->ctClassement = $ctClassement;

        return $this;
    }

    public function getCtAdresse(): ?string
    {
        return $this->ctAdresse;
    }

    public function setCtAdresse(?string $ctAdresse): self
    {
        $this->ctAdresse = $ctAdresse;

        return $this;
    }

    public function getCtComplement(): ?string
    {
        return $this->ctComplement;
    }

    public function setCtComplement(?string $ctComplement): self
    {
        $this->ctComplement = $ctComplement;

        return $this;
    }

    public function getCtCodepostal(): ?string
    {
        return $this->ctCodepostal;
    }

    public function setCtCodepostal(?string $ctCodepostal): self
    {
        $this->ctCodepostal = $ctCodepostal;

        return $this;
    }

    public function getCtVille(): ?string
    {
        return $this->ctVille;
    }

    public function setCtVille(?string $ctVille): self
    {
        $this->ctVille = $ctVille;

        return $this;
    }

    public function getCtCoderegion(): ?string
    {
        return $this->ctCoderegion;
    }

    public function setCtCoderegion(?string $ctCoderegion): self
    {
        $this->ctCoderegion = $ctCoderegion;

        return $this;
    }

    public function getCtPays(): ?string
    {
        return $this->ctPays;
    }

    public function setCtPays(?string $ctPays): self
    {
        $this->ctPays = $ctPays;

        return $this;
    }

    public function getCtTelephone(): ?string
    {
        return $this->ctTelephone;
    }

    public function setCtTelephone(string $ctTelephone): self
    {
        $this->ctTelephone = $ctTelephone;

        return $this;
    }

    public function getCtTelecopie(): ?string
    {
        return $this->ctTelecopie;
    }

    public function setCtTelecopie(string $ctTelecopie): self
    {
        $this->ctTelecopie = $ctTelecopie;

        return $this;
    }

    public function getCtEmail(): ?string
    {
        return $this->ctEmail;
    }

    public function setCtEmail(string $ctEmail): self
    {
        $this->ctEmail = $ctEmail;

        return $this;
    }

    public function getCtSite(): ?string
    {
        return $this->ctSite;
    }

    public function setCtSite(string $ctSite): self
    {
        $this->ctSite = $ctSite;

        return $this;
    }

    public function getReNo(): ?string
    {
        return $this->reNo;
    }

    public function setReNo(?string $reNo): self
    {
        $this->reNo = $reNo;

        return $this;
    }

    public function getNCattarif(): ?int
    {
        return $this->nCattarif;
    }

    public function setNCattarif(?int $nCattarif): self
    {
        $this->nCattarif = $nCattarif;

        return $this;
    }

    public function getTauxremise(): ?string
    {
        return $this->tauxremise;
    }

    public function setTauxremise(?string $tauxremise): self
    {
        $this->tauxremise = $tauxremise;

        return $this;
    }

    public function getTauxescompte(): ?string
    {
        return $this->tauxescompte;
    }

    public function setTauxescompte(?string $tauxescompte): self
    {
        $this->tauxescompte = $tauxescompte;

        return $this;
    }

    public function getTva(): ?string
    {
        return $this->tva;
    }

    public function setTva(string $tva): self
    {
        $this->tva = $tva;

        return $this;
    }

    public function getMrIntitule(): ?string
    {
        return $this->mrIntitule;
    }

    public function setMrIntitule(string $mrIntitule): self
    {
        $this->mrIntitule = $mrIntitule;

        return $this;
    }

    public function getPasswordB2b(): ?string
    {
        return $this->passwordB2b;
    }

    public function setPasswordB2b(string $passwordB2b): self
    {
        $this->passwordB2b = $passwordB2b;

        return $this;
    }

    public function getNewsletter(): ?string
    {
        return $this->newsletter;
    }

    public function setNewsletter(string $newsletter): self
    {
        $this->newsletter = $newsletter;

        return $this;
    }

    public function getCommercial(): ?string
    {
        return $this->commercial;
    }

    public function setCommercial(string $commercial): self
    {
        $this->commercial = $commercial;

        return $this;
    }

    public function getCtType(): ?string
    {
        return $this->ctType;
    }

    public function setCtType(string $ctType): self
    {
        $this->ctType = $ctType;

        return $this;
    }
}
