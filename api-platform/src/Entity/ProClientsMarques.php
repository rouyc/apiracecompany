<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ProClientsMarquesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=ProClientsMarquesRepository::class)
 */
class ProClientsMarques
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $ct_num;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $marque;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCtNum(): ?string
    {
        return $this->ct_num;
    }

    public function setCtNum(string $ct_num): self
    {
        $this->ct_num = $ct_num;

        return $this;
    }

    public function getMarque(): ?string
    {
        return $this->marque;
    }

    public function setMarque(string $marque): self
    {
        $this->marque = $marque;

        return $this;
    }
}
